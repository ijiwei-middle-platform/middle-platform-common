## ijiwei-middle-platform-common

数据中台通用库

## 配置 composer

composer.json
 ```
    "repositories": [
        {
            "type": "git",
            "url": "https://gitee.com/ijiwei-middle-platform/middle-platform-common.git"
        }
    ],
```    

## 安装：
```
composer require "ijiwei/middle-platform-common"
```