<?php

namespace Tests\Feature;

use Assert\Assertion;
use Ijiwei\MiddlePlatform\Common\Helpers\SignHelper;
use Ijiwei\MiddlePlatform\Common\Services\SignService;
use Ijiwei\MiddlePlatform\Common\Utils\HttpUtil;
use Ijiwei\MiddlePlatform\Common\Utils\Util;
use Ijiwei\MiddlePlatform\Weaver\Utils\FwConfig;
use PHPUnit\Framework\TestCase;

final class SignTest extends TestCase
{

    public function getData(){
        $data = [
            'appid'=>'app101',//appid必填
            'total_fee'=>0.01,//订单金额，单位元
            'biz_order_no' => "hbd-1639539154",//商品订单号 必填 建议22位数字
            'timestamp'=> 1639539154,//当前时间戳
            'version'=>'1.0',//版本号
            'sign' => '46F26AAF6BD78E0BD98697C8748447F9',
        ];

        return $data;
    }

    public function getAppInfo(){
        $app = [
            'appid' => 'app101',
            'app_secret' => '994e0ce4c03ddc70cb652f111681c884',
            'debug' => false,
        ];
        return $app;
    }


    /**
     * @test
     */
    public function test1()
    {
        $data = $this->getData();
        $app = $this->getAppInfo();

        $service = new SignService($app);
        $result = $service->sign($data, $app);
        //var_dump($result);

        $rr = $service->validSign($data,$app);
        //var_dump($rr);
        $this->assertTrue($rr,'计算签名错误');
    }


    /**
     * 进行签名验证 [仅供测试]
     * @author wjh 2018-08-23
     */
    public function validSignTest() {

        $data = $this->getData();
        $app = $this->getAppInfo();
        $service = new SignService($app);

        self::logSignLog('传入参数');
        self::logSignLog($data);

        self::logSignLog('app:');
        self::logSignLog($app);

        //$sign = SignHelper::getSign($data, $app['app_secret']);
        $sign = $service->sign($data);
        self::logSignLog('第三步：sign MD5(stringSignTemp):');
        self::logSignLog($sign);

        $z = $data['sign'];
        unset($data['sign']);
        $data['key'] = $app['app_secret'];

        self::logSignLog('第四步：完整URL');
        self::logSignLog(HttpUtil::ToUrlParams($data) . '&sign=' . $z);

        $data['sign'] = $z;

        $this->assertTrue($sign == $data['sign'],'验签错误');

        if ($sign == $data['sign']) {
            self::logSignLog('验签正确');
            return true;
        } else {
            self::logSignLog('验签错误');
            throw new \Exception('验签错误,传入 '. $data['sign'] . ' 计算 ' . $sign);
        }
    }


    /**
     * 计算签名 [仅供测试]
     * @author wjh 2018-08-23
     * @test
     */
    public function signTest() {

        $data = $this->getData();
        $app = $this->getAppInfo();
        $service = new SignService($app);

        self::logSignLog('传入参数');
        self::logSignLog($data);

        Assertion::notEmpty($data['appid'], 'appid is empty');
        //Assertion::notEmpty($data['nonce_str'], 'nonce_str is empty');
        Assertion::notEmpty($data['timestamp'], 'timestamp is empty');
        Assertion::notEmpty($data['version'], 'version is empty');
        //Assertion::notEmpty($data['sign'], 'sign is empty');

        //验签
        //$app = \ThirdAppModel::findEnabledByAppId($data['appid']);
        Assertion::notEmpty($app, 'app is empty');

        self::logSignLog('app:');
        self::logSignLog($app);

        $sign = $service->sign($data, $app['app_secret']);
        self::logSignLog('第三步：sign MD5(stringSignTemp):');
        self::logSignLog($sign);

        //return $sign;
        $this->assertTrue(!empty($sign),'计算签名错误');

    }

    /**
     * 输出日志
     * @author wjh 2018-08-23
     * @param string $msg 消息
     */
    public static function logSignLog($msg){
        Util::print_rw($msg);
        //LogService::logFile('log-service.log',$msg);
    }



}