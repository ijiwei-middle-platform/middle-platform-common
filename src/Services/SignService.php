<?php

namespace Ijiwei\MiddlePlatform\Common\Services;

use Assert\Assert;
use Assert\Assertion;
use Ijiwei\MiddlePlatform\Common\Helpers\SignHelper;
use Ijiwei\MiddlePlatform\Common\Utils\HttpUtil;
use Ijiwei\MiddlePlatform\Common\Utils\Util;

/**
 * 签名服务
 * @author wjh 2018-08-23
 */
class SignService extends BaseService
{

    private array $config;

    /**
     * SignService constructor.
     * @param null $config
     * @throws \Assert\AssertionFailedException
     */
    public function __construct($config) {
        Assertion::notEmpty($config['appid'], 'appid is empty');
        Assertion::notEmpty($config['app_secret'], 'app_secret is empty');

        $defaultConfig = [
            'debug'   => false,
            'version' => '1.0'
        ];
        $this->config = Util::setDefaultValue($config, $defaultConfig);
    }


    /**
     * 进行签名验证
     * @param array $data 传入数据
     * @return bool
     * @throws \Assert\AssertionFailedException
     * @throws \Exception
     * @author wjh 2018-08-23
     *
     */
    public function validSign(array $data) {
        //unset($data['_url']);
        $app = $this->config;

        $this->logSignLog('传入参数');
        $this->logSignLog($data);

        Assertion::notEmpty($data['appid'], 'appid is empty');
        //Assertion::notEmpty($data['nonce_str'], 'nonce_str is empty');
        Assertion::notEmpty($data['timestamp'], 'timestamp is empty');
        Assertion::notEmpty($data['version'], 'version is empty');
        Assertion::notEmpty($data['sign'], 'sign is empty');

        //验签
        //$app = \ThirdAppModel::findEnabledByAppId($data['appid']);
        Assertion::notEmpty($app, 'app is empty');

        $this->logSignLog('app:');
        $this->logSignLog($app);
        $sign = SignHelper::getSign($data, $app['app_secret']);
        if ($sign == $data['sign']) {
            $this->logSignLog('验签正确');
            return true;
        } else {
            $this->logSignLog('验签错误');
            throw new \Exception('验签错误,传入[' . $data['sign'] . '] 正确值 [' . $sign . ']');
        }
    }


    /**
     * 计算签名
     * @param array $data 传入数据
     * @return bool
     * @throws \Assert\AssertionFailedException
     * @throws \Exception
     * @author wjh 2018-08-23
     *
     */
    public function sign(array $data) {
        $app = $this->config;

        $this->logSignLog('传入参数');
        $this->logSignLog($data);

        Assertion::notEmpty($data['appid'], 'appid is empty');
        //Assertion::notEmpty($data['nonce_str'], 'nonce_str is empty');
        Assertion::notEmpty($data['timestamp'], 'timestamp is empty');
        Assertion::notEmpty($data['version'], 'version is empty');
        //Assertion::notEmpty($data['sign'], 'sign is empty');

        //验签
        //$app = \ThirdAppModel::findEnabledByAppId($data['appid']);
        Assertion::notEmpty($app, 'app is empty');

        $this->logSignLog('app:');
        $this->logSignLog($app);

        $sign = SignHelper::getSign($data, $app['app_secret']);
        $this->logSignLog('第三步：sign MD5(stringSignTemp):');
        $this->logSignLog($sign);

        return $sign;
    }


    /**
     * 输出日志
     * @param string $msg 消息
     * @author wjh 2018-08-23
     */
    public function logSignLog($msg) {
        if ($this->config['debug'] === true) {
            Util::print_rw($msg);
            //LogService::logFile('log-service.log',$msg);
        }
    }


}