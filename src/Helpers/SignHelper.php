<?php

namespace Ijiwei\MiddlePlatform\Common\Helpers;


use Ijiwei\MiddlePlatform\Common\Utils\HttpUtil;

/**
 * Class SignHelper
 * 签名帮助类, 参照微信签名算法
 * @author wjh 2018-08-23
 *
 * @package Common\Helpers
 */
class SignHelper
{

    /**
     * 验签
     * @author wjh 2018-08-22
     * @param $array
     * @param $appkey
     * @return bool
     * @throws \Exception
     */
    public static function validSign($array,$appkey){
        $signRsp = $array["sign"];
        $sign = self::getSign($array,$appkey);
        if($sign==$signRsp){
            return true;
        }
        else {
            throw new \Exception("验签失败:".$signRsp."--".$sign);
        }
        return false;
    }


    /**
     * 获取签名
     *
     * @author wjh 2018-08-22
     * @param $array
     * @param $appkey
     * @return string
     */
    public static function getSign($array,$appkey){
        unset($array['sign']);
        unset($array['_url']);
        //$array['key']=$appkey;

        ksort($array);
        $str = HttpUtil::ToUrlParams($array);
        $signTemp = $str . '&key=' . $appkey;
        $sign = strtoupper(md5($signTemp));
        return $sign;
    }

    /**
     * 获取签名
     *
     * @author wjh 2018-08-22
     * @param $array
     * @param $appkey
     * @return string
     */
    public static function testSign($array,$appkey){
        //$msg = '{"Barcode":"184GCQ201247","FillInAmount":"1","OutTradeNo":"1222","AuthCode":"134545196237173892","Sign":"44350C1939A53316FBF0A6630AE3FBEA"}';
        $array = [
            '_url'=> 'ssss',
            'Barcode'=>'184GCQ201247',
            'FillInAmount'=>'1',
            'OutTradeNo'=>'1222',
            'AuthCode'=>'134545196237173892',
            'Key'=>'000000020004'
        ];

        $array['sign'] = self::getSign($array,$array['key']);
        print_r2($array);
    }


    /**
     *
     * 产生随机字符串，不长于32位
     * @param int $length
     * @return 产生的随机字符串
     */
    public static function getNonceStr($length = 32) {
        $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        $str = "";
        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }
        return $str;
    }

    /**
     * 获取毫秒级别的时间戳
     */
    public static function getMillisecond() {
        //获取毫秒的时间戳
        $time = explode(" ", microtime());
        $time = $time[1] . ($time[0] * 1000);
        $time2 = explode(".", $time);
        $time = $time2[0];
        return $time;
    }


}