<?php

namespace Ijiwei\MiddlePlatform\Common\Utils;

/**
 * 帮助类
 */
class JsonUtil
{



    /**
     * 判断字符串是否json字符串
     * @author wjh 2018-09-07
     * @param $value
     * @return bool
     */
    public static function isJsonString($value) {
        if (null === \json_decode($value) && JSON_ERROR_NONE !== \json_last_error()) {
            return false;
        }
        return true;
    }




}
