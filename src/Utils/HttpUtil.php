<?php

namespace Ijiwei\MiddlePlatform\Common\Utils;

/**
 * 帮助类
 */
class HttpUtil
{

    /**
     * 获取测试数据
     * @param array $data 数据
     * @param string $type 类型 array,json
     * @return array|false|string
     */
    public static function getTest($data = [], $type = 'json') {
        $result = [
            'id'     => rand(1000, 9999),
            'data'   => $data,
            'ts'     => time()
        ];

        if ($type == 'json')
            $data = json_encode($result);

        return $data;
    }

    /**
     * 转换数组为 URL 参数形式  如：a=1&b=2
     * @author wjh 2018-01-16
     * @param array $array
     * @return string
     */
    public static function ToUrlParams(array $array) {
        $buff = "";
        foreach ($array as $k => $v) {
            if ($v != "" && !is_array($v)) {
                $buff .= $k . "=" . $v . "&";
            }
        }

        $buff = trim($buff, "&");
        return $buff;
    }

    /**
     * 转换数组为 URL 参数形式  如：a=1&b=2
     * @author wjh 2018-01-16
     * @param array $array
     * @return string
     */
    public static function ToUrlParamsWithEncode(array $array) {
        $buff = "";
        foreach ($array as $k => $v) {
            if ($v != "" && !is_array($v)) {
                $buff .= $k . "=" .urlencode($v) . "&";
            }
        }

        $buff = trim($buff, "&");
        return $buff;
    }


    /**
     * 格式化变量列表以便输出日志
     * @author wjh 2019-07-18
     *
     * @param array $args
     * @return array
     */
    public static function format_args(...$args) {
        $data = array_map(function ($value) {
            $result = null;
            if (is_array($value))
                $result= json_encode($value);
            elseif (is_object($value))
                $result= json_encode($value);
            elseif (is_bool($value))
                $result = $value === true ? 'true' : 'false';
            else
                $result = $value;

            return $result;
        }, $args);

        return $data;
    }



}
