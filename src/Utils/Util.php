<?php

namespace Ijiwei\MiddlePlatform\Common\Utils;

/**
 * 帮助类
 */
class Util extends BaseUtil
{

    /**
     * 获取当前日期
     * @author wjh 2021-12-04
     * @param string $format
     * @return false|string
     */
    public static function getCurrentDate($format = 'Y-m-d') {
        date_default_timezone_set('PRC');
        return date($format, time());
    }


    /**
     * 获取当前时间
     * @param string $format
     * @return false|string
     * @author wjh 2021-12-04
     */
    public static function getCurrentTime($format = 'Y-m-d H:i:s') {
        date_default_timezone_set('PRC');
        return date($format, time());
    }


    /**
     * 获取日期
     * @param $dt
     * @return false|string|null
     * @author wjh
     * @version 2014-4-26
     */
    public static function getDate($dt) {
        if (empty($dt))
            return null;

        return date('Y-m-d', strtotime($dt));
    }


    /**
     * 获取当前时间
     * @param $time
     * @param string $format
     * @return false|string
     */
    public static function getTimeFromTimestamp($time, $format = 'Y-m-d H:i:s') {
        if (empty($time))
            $time = time();

        date_default_timezone_set('PRC');
        return date($format, $time);
    }


    /**
     * 获取当前时间数值(包含毫秒)
     * @return string
     * @author wjh 2017-08-03
     */
    public static function getCurrentMicroTime() {
        list($s1, $s2) = explode(' ', microtime());
        $dt = date('Y-m-d H:i:s', floatval($s2));
        $dt2 = substr($s1, 2, 3);
        return "{$dt} {$dt2}";
    }


    /**
     * 获取当前时间戳(秒级), 如传入 start 则计算2者的差值
     * @author wjh 20141204
     * @param int $start
     * @return float
     */
    public static function getSecond($start = 0) {
        return time() - $start;
    }


    /**
     * 获取当前时间戳(毫秒级)，如传入 start 则计算2者的差值
     * @author wjh 20141204
     * @param int $start
     * @return float
     */
    public static function getMillisecond($start=0) {
        list($s1, $s2) = explode(' ', microtime());
        $now = (float)sprintf('%.0f', (floatval($s1) + floatval($s2)) * 1000);
        return $now-$start;
    }


    /**
     * set array with default value
     * @param array $data
     * @param array $defaultValue
     * @return array
     * @author wjh 2021-09-20
     */
    public static function setDefaultValue(array $data, array $defaultValue) {
        foreach ($defaultValue as $key => $value) {
            if (!isset($data[$key]))
                $data[$key] = $value;
        }
        return $data;
    }


    /**
     * merge data to array
     * @param array $data
     * @param array $mergeValue
     * @return array
     * @author wjh 2021-12-28
     */
    public static function setMergeValue(array $data, array $mergeValue) {
        foreach ($mergeValue as $key => $value) {
            $data[$key] = $value;
        }
        return $data;
    }


    /**
     * 在输出中添加<pre>
     * @author wjh
     * @version 2014-4-30
     */
    public static function addPreStart() {
        print "<pre>";
    }

    /**
     * 在输出中添加</pre>
     * @author wjh
     * @version 2014-4-30
     */
    public static function addPreEdn() {
        print "</pre>";
    }



    /**
     * print_r for console
     * @author wjh 2020-09-24
     * @param mixed ...$args
     */
    public static function print_rc(...$args) {
        $defaultConfig = [
            'pre' => true, //是否有前缀
            'preChar' => PHP_EOL, //前缀字符
            'split' => true, //是否元素有前缀
            'splitChar' => PHP_EOL, //元素字符
            'end' => true, //是否有后缀
            'endChar' => PHP_EOL, //后缀字符
        ];
        return self::print_rc_ext($args, $defaultConfig);
    }


    /**
     * print_r for console extend
     * @param array $args
     * @param array $config
     * @author wjh 2020-09-24
     */
    public static function print_rc_ext(array $args, array $config = []) {
        $defaultConfig = [
            'pre' => true, //是否有前缀
            'preChar' => PHP_EOL, //前缀字符
            'split' => true, //是否元素有前缀
            'splitChar' => PHP_EOL, //元素字符
            'end' => true, //是否有后缀
            'endChar' => PHP_EOL, //后缀字符
        ];
        $config = Util::setDefaultValue($config, $defaultConfig);

        if ($config['pre'])
            echo $config['preChar'];
        array_map(function ($item) use ($config) {
            $split = $config['split'] ? $config['splitChar'] : '';
            if (is_array($item))
                print_r($item);
            else
                echo $item;

            echo($split);
        }, $args);
        if ($config['end'])
            echo $config['endChar'];
    }


    /**
     * print_r for web
     * @author wjh 2014-7-1
     * @param $expression
     * @param null $return
     */
    public static function print_rw(...$expression){
        $defaultConfig = [
            'pre'       => true, //是否有前缀
            'preChar'   => '<pre>', //前缀字符
            'split'     => true, //是否元素有前缀
            'splitChar' => '<br>', //元素字符
            'end'       => true, //是否有后缀
            'endChar'   => '</pre>', //后缀字符
        ];
        return self::print_rw_ext($expression, $defaultConfig);
    }


    /**
     * print_r for web extend
     * @param array $expression
     * @param array $config
     * @example print_rw_ext(
     *  [
     *      123,
     *      456
     *  ],
     *  [
     *      'line'=> true   //生成新行
     *  ])
     * @author wjh 2014-7-1
     */
    public static function print_rw_ext(array $expression, array $config = []) {

        $defaultConfig = [
            'pre'       => true, //是否有前缀
            'preChar'   => '<pre>', //前缀字符
            'split'     => true, //是否元素有前缀
            'splitChar' => '<br>', //元素字符
            'end'       => true, //是否有后缀
            'endChar'   => '</pre>', //后缀字符
        ];
        $config = Util::setDefaultValue($config, $defaultConfig);

        if ($config['pre'])
            echo $config['preChar'];
        array_map(function ($item) use ($config) {
            $split = $config['split'] ? $config['splitChar'] : '';
            print_r($item);
            print_r($split);

        }, $expression);
        if ($config['end'])
            echo $config['endChar'];
    }



    /**
     * 判断变量true/false
     * @author wjh 2018-09-07
     * @param mixed $value
     * @return bool
     */
    public static function evalBool($value) {
        if ($value)
            return true;
        else
            return false;
    }


}
