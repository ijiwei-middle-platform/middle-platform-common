<?php

namespace Ijiwei\MiddlePlatform\Common\Utils;

/**
 * 帮助类
 */
class StringUtil
{


    /**
     * startWith
     * @author wjh 2014-6-17
     * @param string $str 原字符串
     * @param string $strart 搜索字符串
     * @return bool
     */
    public static function startWith($str, $strart)
    {
        if (empty($str) || empty($strart)) {
            return false;
        }

        $str = substr($str, 0, strlen($strart));
        return $str == $strart;
    }


    /**
     * removeStartWith
     * @author wjh 2020-07-24
     * @param string $str 原字符串
     * @param string $start 搜索字符串
     * @return bool
     */
    public static function removeStartWith($str, $start)
    {
        if (empty($str) || empty($start)) {
            return $str;
        }

        $start2 = substr($str, 0, strlen($start));
        if($start2 == $start)
            return substr($str,strlen($start));
        else
            return $str;
    }



}
